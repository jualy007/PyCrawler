# -*- coding: utf-8 -*-

import os
from scrapy import cmdline

if __name__ == '__main__':
    name = 'taobaoCrawl'
    home = os.path.dirname(__file__)

    #scrapy crawl must running under project root folder
    os.chdir(home)
    cmdline.execute(['scrapy', 'crawl', name])