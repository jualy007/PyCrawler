# -*- coding: utf-8 -*-
import scrapy


class ExampleSpider(scrapy.Spider):
    name = 'example1'

    def start_requests(self):
        urls = [
            'http://quotes.toscrape.com/tag/books/',
            'http://quotes.toscrape.com/tag/life/'

            'https://s.taobao.com/search?q=UCC%E6%8C%82%E8%80%B3%E5%92%96%E5%95%A1&imgfile=&js=1&stats_click=search_radio_all%3A1&initiative_id=staobaoz_20180705&ie=utf8'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        self.log('Response : {0}'.format(response))
        page = response.url.split("/")[-2]
        filename = 'quotes-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)

    @classmethod
    def from_crawler(cls, crawler):
        pass
