# -*- coding: utf-8 -*-
import scrapy
import re
import urllib3
import json
import random

from PyCrawler.items import PycrawlerItem


class ExampleSpider(scrapy.Spider):
    name = 'taobaoCrawl'
    allowed_domains = ["taobao.com"]

    def start_requests(self):
        key = "UCC挂耳咖啡"

        search_key = str(bytes(key, encoding='utf-8'))[2:-1].replace(
            '\\x', '%').upper()

        for index in range(2):
            url = 'https://s.taobao.com/search?q={0}&js=1&stats_click=search_radio_all%3A1&ie=utf8&s={1}'.format(
                search_key, 44 * index)

            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        self.log('Response : {0}'.format(response))
        body = response.body.decode('utf-8', 'ignore')
        pattam_id = '"nid":"(.*?)"'
        all_nid = re.compile(pattam_id).findall(body)

        for nid in all_nid:
            proc_url = 'https://item.taobao.com/item.htm?id=' + str(nid)

            yield scrapy.Request(url=proc_url, callback=self.processProc)

    def processProc(self, response):
        self.log('Response : {0}'.format(response))
        body = response.body.decode('utf-8', 'ignore')

        item = PycrawlerItem()

        procUrl = response.url

        pattam_url = 'https://(.*?).com'

        subdomain = re.compile(pattam_url).findall(procUrl)

        httpPM = urllib3.PoolManager()

        headers = {
            'accept': '*/*',
            'accept-encoding': 'gzip, deflate, sdch, br',
            'accept-language': 'zh-CN,zh;q=0.8',
            'referer': procUrl,
            'user-agent': response.request.headers.get('User-Agent').decode()
        }

        if not subdomain[0] == 'item.taobao':
            procName = response.xpath(
                "//div[@class='tb-detail-hd']/h1/text()").extract()
            procPrice = response.xpath(
                "//span[@class='tm-price']/text()").extract()

            shopName = response.xpath(
                "//a[@class='slogo-shopname']/strong/text()").extract()[0]
            shopUrl = response.xpath(
                "//a[@class='slogo-shopname']/@href").extract()[0]
            shopOwner = response.xpath(
                "//li[@class='shopkeeper']/div/a/text()").extract()[0]

            procCode = procUrl.split('?')[1].split('=')[1].strip()

            pattam_sellerId = '&sellerId=(.*)&'
            sellerId = re.compile(pattam_sellerId).findall(response)[0]

            pattam_initapi = '"initApi":"(.*?)",'
            initapi_url = 'https:' + re.compile(pattam_initapi).findall(
                response)[0]
            initapi_data = httpPM.request(
                'GET', initapi_url, headers=headers).data.decode(
                    'utf-8', 'ignore')
            pattam_tmcount = '"sellCount":(.*?),'
            procTmCount = re.compile(pattam_tmcount).findall(initapi_data)[0]

            comment_url = 'https://dsr-rate.tmall.com/list_dsr_info.htm?itemId={0}&sellerId={1}'.format(
                procCode, sellerId)
            comment_data = httpPM.request(
                'GET', comment_url, headers=headers).data.decode(
                    'utf-8', 'ignore')

            pattam_comment = '"rateTotal":(.*?)}'
            procComment = re.compile(pattam_comment).findall(comment_data)[0]
        else:
            procName = response.xpath(
                "//h3[@class='tb-main-title']/@data-title").extract()
            procPrice = response.xpath(
                "//em[@class = 'tb-rmb-num']/text()").extract()

            pattam_config = 'var g_config = ({[.\s\S]*?});'
            orgConfig = re.compile(pattam_config).findall(body)[0]

            #clear \n and space
            fixConfig = re.sub('\n +', '', orgConfig)
            fixConfig = re.sub('  +', '', fixConfig)

            #clear unnormal type
            fixConfig = fixConfig.replace('startTime: +new Date,', '')
            fixConfig = fixConfig.replace('disableAddToCart: !true,', '')
            fixConfig = re.sub('descUrl: .*?,', '', fixConfig)
            fixConfig = re.sub('lgurl: .*?,', '', fixConfig)

            fixConfig = fixConfig.replace('\'', '"')
            fixConfig = re.sub(r'([a-zA-Z_]*?) ?:', r'"\1":', fixConfig)
            global_config = json.loads(fixConfig, encoding='utf-8')

            procCode = global_config.get('itemId')
            shopName = global_config.get('shopName')
            shopUrl = global_config.get('idata').get('shop').get('url')
            shopId = global_config.get('idata').get('shop').get('id')
            shopOwner = global_config.get('sellerNick')
            sibUrl = 'https:' + global_config.get('sibUrl').strip()

            sell_data = httpPM.request(
                'GET', sibUrl, headers=headers).data.decode('utf-8', 'ignore')

            pattam_quantity = '"soldQuantity":{"confirmGoodsCount":(.*?),"soldTotalCount":(.*?)}'
            soldQuantity = re.compile(pattam_quantity).findall(sell_data)
            procTmCount = '{0}/{1}'.format(soldQuantity[0][0],
                                           soldQuantity[0][1])

            comment_url = 'https://rate.taobao.com/detailCount.do?itemId=' + str(
                procCode)
            comment_data = httpPM.request(
                'GET', comment_url, headers=headers).data.decode(
                    'utf-8', 'ignore')
            pattam_comment = '{"count":(.*?)}'
            procComment_total = re.compile(pattam_comment).findall(
                comment_data)[0]

            detail_url = 'https://rate.taobao.com/detailCommon.htm?auctionNumId={0}&userNumId={1}'.format(
                procCode, shopId)
            detail_data = httpPM.request(
                'GET', detail_url, headers=headers).data.decode(
                    'utf-8', 'ignore')
            detail_data = re.sub('\r\n', '', detail_data)
            detail_count = json.loads(
                detail_data[1:-1]).get('data').get('count')
            detail_good = detail_count.get('goodFull')
            detail_normal = detail_count.get('normal')
            detail_bad = detail_count.get('bad')
            procComment = '{0}/{1}/{2}/{3}'.format(
                procComment_total, detail_good, detail_normal, detail_bad)

        item['shop_name'] = shopName.strip()
        item['shop_url'] = shopUrl.strip()
        item['shop_owner'] = shopOwner.strip()

        item['proc_url'] = procUrl.strip()
        item['proc_name'] = procName[0].strip()
        item['proc_price'] = procPrice[0].strip()
        item['proc_comment'] = procComment
        item['proc_tmCount'] = procTmCount
        item['proc_code'] = procCode

        yield item